package ru.javarush.crud.dao;

import ru.javarush.crud.model.User;

import java.util.List;

public interface UserDao {
    void addUser(User user);
    void updateUser(User user);
    void removeUser(int id);
    User getUser(int id);
    List<User> getUsers();
    List<User> getUsers(String name);

}
