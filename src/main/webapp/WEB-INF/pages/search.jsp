<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page session="false" %>

<html>
<head>
    <title>S User Data</title>
    <style>
        <%@include file='frame.css' %>
        <%@include file='style.css' %>
    </style>
</head>
<body>
<a href="<c:url value="/users"/>">Back to users list</a>
<br/>
<br/>
<h1>Search User</h1>
<c:url var="searchuser" value="/searchresults"/>
<br/>

<form:form action="${searchuser}" commandName="searcheduser">
    <table>
        <tr>
            <td><spring:message text="Name"/><form:input path="name"/> </td>
            <td><input class="but" type="submit" name="action" value="<spring:message text="Search"/>"/></td>
        </tr>
    </table>
</form:form>
</body>
</html>